#pragma once
#include <map>
#include <string>
#include <vector>
#include <boost/python.hpp> 

using namespace boost::python;
using std::string;
using std::map;
using std::vector;

class Block
{
public:
	explicit Block(const char * file_name);
	boost::python::list get_block_list();
	boost::python::object get_block(const char * block_name);
	boost::python::dict get_all_blocks();

private:
	boost::python::dict dict_blocks_;
};

BOOST_PYTHON_MODULE(tdx)
{
	class_<Block>("Block", init<const char *>())
		.def("get_block_list", &Block::get_block_list)
		.def("get_block", &Block::get_block)
		.def("get_all_blocks", &Block::get_all_blocks);
}
