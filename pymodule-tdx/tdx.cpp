#pragma  pack(1)
#include <fstream>
#include <iostream>
#include <boost/python.hpp> 
#include <boost/locale.hpp>
#include "tdx.h"

using std::ios;
using std::cout;
using std::ifstream;

struct BlockFileHead
{
	char version_info[64];
	int	index_start;
	int data_start;
	int reserved_1;
	int reserved_2;
	int reserved_3;
};

struct BlockInfo
{
	char name[9];
	unsigned short stock_count;
	unsigned short block_level;
	char stock_code[7*400];
};

Block::Block(const char * file_name)
{
	ifstream infile(file_name, ios::binary);
	if (!infile)
		return;

	BlockFileHead head;
	infile.read((char*)&head, sizeof(struct BlockFileHead));
	//cout << "version_info:" << head.version_info;
	//cout << "data_start:" << head.data_start;

	unsigned short block_count;
	infile.seekg(head.data_start);
	infile.read((char *)&block_count, sizeof(unsigned short));
	//cout << "block_count:" << block_count;

	BlockInfo data;
	boost::python::list stock_list;
	while (!infile.eof())
	{
		infile.read((char *)&data, sizeof(BlockInfo));
		const char * p = data.stock_code;

		//clear stock_list first
		del(stock_list[slice()]);
		for (int i = 0; i < data.stock_count; i++)
		{
			stock_list.append(p);
			p += 7;
		}

		//encode the block name with UTF-8
		string utf8_name;
		utf8_name = boost::locale::conv::to_utf<char>(data.name, "GBK");

		//put into the dict
		dict_blocks_[utf8_name] = stock_list;
	} //while

	infile.close();
	return;
}

boost::python::list Block::get_block_list()
{
	return	dict_blocks_.keys();
}

boost::python::object Block::get_block(const char * block_name)
{
	//boost::python::list stock_list = dict_blocks_.get(block_name)
	return dict_blocks_.get(block_name);
}

boost::python::dict Block::get_all_blocks()
{
	return	dict_blocks_;
}

